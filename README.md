# Reddit Top 2.5 Million

## What is this?

This is a datase

Who knows! Here's one simple example: [This is a breakdown of the top voted domains on Serendipity.](https://docs.google.com/spreadsheet/ccc?key=0AmvRNMJOaKWldDA4TlBqNHcyOU85ZmRXUzNNRE5lR2c#gid=2) (Yes, as a pie chart.)

For my use, I'm considering finding important terms for a subreddit using [TF-IDF](http://en.wikipedia.org/wiki/Tf*idf).

Have other ideas? I'd love to hear what you do with this. [Let me know.](https://twitter.com/chrisdary)

## Gotchas?

Yes, one: This only includes subreddits that are *not marked NSFW*. This is related to my particular use case, in which NSFW subreddits aren't useful for me. They'd be relatively easy to scrape if you're interested, you'll just need to get the list of NSFW subreddits from http://www.reddit.com/reddits. 

